import React, { useState } from 'react';
import { useEffect } from 'react/cjs/react.development';
import "./Board.css";

//linked list Node class creation 
class LinkedListNode{
    constructor(value) {
        this.value = value;
        //when created node should not be pointing to anything
        this.next = null;
    }
}

class LinkedList{
    constructor(value) {
        var node = new LinkedListNode(value);
        this.head = node;
        this.tail = node;
    }
}

class Cell{
    constructor(row, column, cell) {
        this.row = row;
        this.column = column;
        this.cell = cell;
    }
}

//board size manually set to 15
const BOARD_SIZE = 15;

function getSnakeFirstCoordForLinkedList(board) {
    const snakeHeadRow = Math.floor(BOARD_SIZE / 2);
    const snakeHeadCol = Math.floor(BOARD_SIZE / 2);
    const cellValueOfHead = board[snakeHeadRow][snakeHeadCol];

    return new Cell(snakeHeadRow, snakeHeadCol, cellValueOfHead);
}

//traverse the linked list and get the cell value from all the nodes
function getSnakeCellsInSet(snake) {
    var current = snake.head;
    var snakeCellValues = new Set();
    while (current != null) {
        snakeCellValues.add(current.value.cell);
        current = current.next;
    }
    return snakeCellValues;
}



//The Board component 
function Board() {
    const [board, setBoard] = useState(
        CreateBoard(BOARD_SIZE)
    );

    //creating a linked list for the snake 
    const [snake, setSnake] = useState(new LinkedList(getSnakeFirstCoordForLinkedList(board)));
    //the snakeCells variable is for our easy purpose of passing the snake cell values to the className function to render snake
    const [snakeCells, setsnakeCells] = useState(getSnakeCellsInSet(snake));
    //hardcoding for debugging purpose
    const [foodCell, setfoodCell] = useState(119);
    //state variable to score
    const [CurrentScore, setCurrentScore] = useState(0);
    // direction state variable to automatically move snake
    const [Direction, setDirection] = useState(Directions.Right);

    // this hook will be called after the react renders for the first time or updates tha dom updates are done.
    useEffect(() => {
        window.addEventListener("keydown", (e) => {
            //e is passed to the function to identify the key pressed in the handle function
            var snakeNextDirection = handleKeyDown(e, snake);
            console.log(snakeNextDirection);
            if (snakeNextDirection != null) {
                console.log(`got direction ${snakeNextDirection}`);
                setDirection(snakeNextDirection);
                console.log(`hello: ${Direction}`);
                HandleSnakeMove();
            }
        });
    }, []);
    
    function HandleSnakeMove() {
        //get current head of the snake
        const currentHeadOfSnake = {
            row: snake.head.value.row,
            column: snake.head.value.column
        };
        console.log(currentHeadOfSnake.row, currentHeadOfSnake.column);
        const nextHeadOfSnake = getNextCoordinatesOfSnakeHead(currentHeadOfSnake, Direction);

        console.log(nextHeadOfSnake.row, nextHeadOfSnake.column);
        //getting the cell values in the board
        const nextHeadCellValue = board[nextHeadOfSnake.row][nextHeadOfSnake.column];

        const newHead = new LinkedListNode(new Cell(nextHeadOfSnake.row, nextHeadOfSnake.column, nextHeadCellValue));

        var currentHead = snake.head;
        snake.head = newHead;
        currentHead.next = newHead;

        const newSnakeCells = new Set();
        newSnakeCells.add(nextHeadCellValue);
        setsnakeCells(newSnakeCells);
    }

    var handleClick = () => {
        HandleSnakeMove();
    }

    return (
        <React.Fragment>
            <h1>Score: {CurrentScore}</h1>
            <button onClick={ handleClick } >Move manually </button>
            <div className="board">{
                board.map((row, rowId) => {
                    return <div className="row" key={rowId}>{
                        row.map((cell, cellId) => {
                            var CellClassName = getCellClassName(foodCell, snakeCells, cell);
                            return <div className={ CellClassName } key={cellId}></div>
                        })
                }</div>
            })
            }</div>
        </React.Fragment>
    );
}

function CreateBoard(boardSize) {
    //perhaps a better way of writing i guess
    // var board = new Array(BOARD_SIZE).fill(0).map((row) => {
    //     return new Array(BOARD_SIZE);
    // });

    var counter = 1;
    var board = [];
    for (var i = 0; i < boardSize; i++){
        var currentRowArray = [];
        for (var j = 0; j < boardSize; j++){
            currentRowArray.push(counter);
            counter = counter + 1;
        }
        board.push(currentRowArray);
    }
    return board;
}

//function to return the className for the cells with different background colors by checking if the cell is supposed to be a food cell or snake cell
var getCellClassName = function (foodcell = -1, snakeCells, cellValue) {
    var cellClassName = "cell";
    if (cellValue === foodcell) {
        cellClassName = "cell cell-food";
    }
    if (snakeCells.has(cellValue)) {
        cellClassName = "cell cell-snake";
    }
    return cellClassName;
}

const Directions = {
    Up: "Up",
    Down: "Down",
    Left: "Left",
    Right: "Right",
  };
  
  function handleKeyDown(event) {
    const nextDirectionOfSnake = getNextDirectionOfSnake(event.code);
    if (nextDirectionOfSnake != null) {
        return nextDirectionOfSnake;
    }
      return null;
  }
  
  function getNextDirectionOfSnake(keycode) {
    if (keycode === "ArrowUp" || keycode === "KeyW") return Directions.Up;
    if (keycode === "ArrowDown" || keycode === "KeyS") return Directions.Down;
    if (keycode === "ArrowLeft" || keycode === "KeyA") return Directions.Left;
    if (keycode === "ArrowRight" || keycode === "KeyD") return Directions.Right;
    return null;
  }
  

function getNextCoordinatesOfSnakeHead(currentHeadCoordinates, direction) {
    console.log(direction);
    if (direction == Directions.Right) {
        console.log(Directions.Right);
        currentHeadCoordinates.column = currentHeadCoordinates.column + 1;
        return currentHeadCoordinates;
    }
    else if (direction == Directions.Left) {
        console.log(Directions.Left);
        currentHeadCoordinates.column = currentHeadCoordinates.column - 1;
        return currentHeadCoordinates;
    }
    else if (direction == Directions.Up) {
        console.log(Directions.Up)
        currentHeadCoordinates.row = currentHeadCoordinates.row - 1;
        return currentHeadCoordinates;
    }
    else if (direction == Directions.Down) {
        console.log(Directions.Down);
        currentHeadCoordinates.row = currentHeadCoordinates.row + 1;
        return currentHeadCoordinates;
    }
  }
  //handle the snake movement
// function handleSnakeMovement(snakeLinkedList, nextDirectionOfSnake) {
//     console.log(snakeLinkedList.head.value.column);
//     if (nextDirectionOfSnake === Directions.Up) {
//         snakeLinkedList.head.value.column = snakeLinkedList.head.value.column + 1;
//     }
//     if (nextDirectionOfSnake === Directions.Down) {
//         snakeLinkedList.head.value.column = snakeLinkedList.head.value.column - 1;
//     }
//     if (nextDirectionOfSnake === Directions.Left) {
//         snakeLinkedList.head.value.row = snakeLinkedList.head.value.row - 1;
//     }
//     if (nextDirectionOfSnake === Directions.Right) {
//         snakeLinkedList.head.value.row = snakeLinkedList.head.value.row + 1;
//     }
//     return snakeLinkedList;
//   }
export default Board;
